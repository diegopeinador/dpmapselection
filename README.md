#DPMapSelection

Manage points on a map easily with this viewcontroller+protocol (you may want to copy the UI from the storyboard as well).

Features:

+   Different pin colors for each type of point (red for initial points, purple for search results and green for manually added ones).
+   Search restricted to the visible area of the map.
+   Optional protocol to customise behaviour and to be notified of the changes.

![screenshot](https://bitbucket.org/diegopeinador/dpmapselection/raw/master/screenshot.png)

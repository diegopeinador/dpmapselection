//
//  DPViewController.m
//  DPMapSelectionDemo
//
//  Created by Diego Peinador on 19/02/14.
//  Copyright (c) 2014 Diego Peinador. All rights reserved.
//

#import "DPViewController.h"
#import "DPMapSelectionViewController.h"

@interface DPViewController ()<DPMapSelectionDelegate>{
    NSMutableArray *_points;
}

@end

@implementation DPViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self.tableView reloadData];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.

    self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    DPPoint *p1 = [[DPPoint alloc] init];
    p1.title = @"Initial point 1";
    [p1 setCoordinate:CLLocationCoordinate2DMake(19.9273, -99.2910)];
    DPPoint *p2 = [[DPPoint alloc] init];
    p2.title = @"Initial point 2";
    [p2 setCoordinate:CLLocationCoordinate2DMake(40.8816, -103.8048)];
    
    _points = [@[p1, p2] mutableCopy];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.destinationViewController isKindOfClass:[DPMapSelectionViewController class]]) {
        DPMapSelectionViewController *mapSelection = segue.destinationViewController;
        mapSelection.delegate = self;
        mapSelection.points = _points;
    }
}

#pragma mark - UITableViewDatasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _points.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PointInfo" forIndexPath:indexPath];
    
    DPPoint *point = [_points objectAtIndex:indexPath.row];
    
    cell.textLabel.text = point.title;
    cell.detailTextLabel.text = point.subtitle;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (editingStyle==UITableViewCellEditingStyleDelete) {
        [_points removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
}

#pragma mark - DPMapSelectionDelegate

-(BOOL)mapSelection:(DPMapSelectionViewController*)mapSelection shouldMovePoint:(DPPoint*)point{
    return point.type != DPPointTypeInitialPoint; // Don't move initial points
}

-(BOOL)mapSelection:(DPMapSelectionViewController*)mapSelection shouldDeletePoint:(DPPoint *)point{
    return point.type != DPPointTypeInitialPoint; // Don't delete initial points
}

-(void)mapSelection:(DPMapSelectionViewController*)mapSelection didAddPoint:(DPPoint*)point{
    [_points addObject:point];
}

-(void)mapSelection:(DPMapSelectionViewController*)mapSelection didDeletePoint:(DPPoint*)point{
    [_points removeObject:point];
}


@end

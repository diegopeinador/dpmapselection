//
//  DPMapSelectionViewController.h
//  DPMapSelectionDemo
//
//  Created by Diego Peinador on 19/02/14.
//  Copyright (c) 2014 Diego Peinador. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@protocol DPMapSelectionDelegate;
@class DPPoint;



@interface DPMapSelectionViewController : UIViewController

@property(nonatomic, copy) NSArray *points; // DPPoint
@property(nonatomic, assign) id<DPMapSelectionDelegate> delegate;

@end



@protocol DPMapSelectionDelegate <NSObject>
@optional
-(BOOL)mapSelection:(DPMapSelectionViewController*)mapSelection shouldMovePoint:(DPPoint*)point;
-(BOOL)mapSelection:(DPMapSelectionViewController*)mapSelection shouldAddPoint:(DPPoint*)point;
-(BOOL)mapSelection:(DPMapSelectionViewController*)mapSelection shouldDeletePoint:(DPPoint*)point;

-(void)mapSelection:(DPMapSelectionViewController*)mapSelection didMovePoint:(DPPoint*)point;
-(void)mapSelection:(DPMapSelectionViewController*)mapSelection didAddPoint:(DPPoint*)point;
-(void)mapSelection:(DPMapSelectionViewController*)mapSelection didDeletePoint:(DPPoint*)point;
@end



typedef enum {
    DPPointTypeUnknown,
    DPPointTypeInitialPoint,
    DPPointTypeSearchResult,
    DPPointTypeDroppedPin,
} DPPointType;



@interface DPPoint : NSObject<MKAnnotation>

@property (nonatomic) DPPointType type;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *subtitle;

@end

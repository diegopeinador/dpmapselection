//
//  DPAppDelegate.h
//  DPMapSelectionDemo
//
//  Created by Diego Peinador on 19/02/14.
//  Copyright (c) 2014 Diego Peinador. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DPAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

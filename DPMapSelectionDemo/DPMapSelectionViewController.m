//
//  DPMapSelectionViewController.m
//  MapSelectionTest
//
//  Created by Diego Peinador on 19/02/14.
//  Copyright (c) 2014 Diego Peinador. All rights reserved.
//

#import "DPMapSelectionViewController.h"
#import <AddressBookUI/AddressBookUI.h>

@interface DPMapSelectionViewController ()<MKMapViewDelegate, UISearchBarDelegate>{
    NSUInteger _droppedPinsCounter;
    UIBarButtonItem *_dropPinButton;
    CLGeocoder *_geoCoder;
    NSString *_previousVCtitle;
    NSMutableArray *_points;
}

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@end

@implementation DPMapSelectionViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    UIViewController *vc = [self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-1];
    vc.navigationItem.title = _previousVCtitle;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    UIViewController *vc = [self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-2];
    _previousVCtitle = vc.navigationItem.title;
    vc.navigationItem.title = @"";
    
    [self resetPointsOnMap];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [self zoomToShowAllAnnotations];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    if (!_points) {
        _points = [NSMutableArray array];
    }
    _droppedPinsCounter = 1;
    _dropPinButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(dropPin:)];
    self.navigationItem.rightBarButtonItem = _dropPinButton;
    _geoCoder = [[CLGeocoder alloc] init];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setPoints:(NSArray *)points{
    if (!_points) {
        _points = [NSMutableArray array];
    }else{
        [_points removeAllObjects];
    }
    for (DPPoint*p in points) {
        p.type = DPPointTypeInitialPoint;
        [_points addObject:p];
    }
}

-(void)resetPointsOnMap{
    [_mapView removeAnnotations:_mapView.annotations];
    for (DPPoint *item in _points) {
        [_mapView addAnnotation:item];
    }
}

-(void)zoomToShowAllAnnotations{
    CGFloat minx=CGFLOAT_MAX, miny=CGFLOAT_MAX, maxx=CGFLOAT_MIN, maxy=CGFLOAT_MIN;
    MKMapPoint point;
    for (id<MKAnnotation>anno in _mapView.annotations) {
        point = MKMapPointForCoordinate(anno.coordinate);
        if (point.x<minx) {
            minx = point.x;
        }
        if (point.x>maxx) {
            maxx = point.x;
        }
        if (point.y<miny) {
            miny = point.y;
        }
        if (point.y>maxy) {
            maxy = point.y;
        }
    }
    MKMapRect mapRect = MKMapRectMake(minx, miny, maxx-minx, maxy-miny);
    [_mapView setVisibleMapRect:mapRect edgePadding:UIEdgeInsetsMake(20, 20, 20, 20) animated:YES];
}

-(void)cancelSearch:(id)sender{
    if (_searchBar.text && ![_searchBar.text isEqualToString:@""]) {
        [self resetPointsOnMap];
    }
    self.navigationItem.rightBarButtonItem = _dropPinButton;
    _searchBar.text = nil;
    [_searchBar resignFirstResponder];
}

-(void)dropPin:(id)sender{
    DPPoint *point = [[DPPoint alloc] init];
    point.type = DPPointTypeDroppedPin;
    point.title = [NSString stringWithFormat:@"%@ %u", NSLocalizedString(@"New location", nil),_droppedPinsCounter++];
    MKMapRect mRect = _mapView.visibleMapRect;
    CLLocationCoordinate2D coor = MKCoordinateForMapPoint(MKMapPointMake(mRect.origin.x+mRect.size.width/2, mRect.origin.y+mRect.size.height/2));
    [point setCoordinate:coor];
    
    BOOL canAddPoint = YES;
    if (_delegate && [_delegate respondsToSelector:@selector(mapSelection:shouldAddPoint:)]) {
        canAddPoint = [_delegate mapSelection:self shouldAddPoint:point];
    }
    if (canAddPoint) {
        CLLocation *loc = [[CLLocation alloc] initWithLatitude:coor.latitude longitude:coor.longitude];
        [_geoCoder reverseGeocodeLocation:loc completionHandler:^(NSArray *placemarks, NSError *error) {
            if (!error && placemarks.count>0) {
                CLPlacemark *place = [placemarks objectAtIndex:0];
                point.subtitle = ABCreateStringWithAddressDictionary(place.addressDictionary, NO);
            }
            if (_delegate && [_delegate respondsToSelector:@selector(mapSelection:didAddPoint:)]) {
                [_delegate mapSelection:self didAddPoint:point];
            }
        }];
        [_points addObject:point];
        [_mapView addAnnotation:point];
    }
}

- (CLLocationDistance)getDistanceFrom:(CLLocationCoordinate2D)start to:(CLLocationCoordinate2D)end
{
	CLLocation *startLoc = [[CLLocation alloc] initWithLatitude:start.latitude longitude:start.longitude];
	CLLocation *endLoc = [[CLLocation alloc] initWithLatitude:end.latitude longitude:end.longitude];
	CLLocationDistance retVal = [startLoc distanceFromLocation:endLoc];
    
	return retVal;
}

#pragma mark - UISearchBarDelegate

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar{
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"CANCEL", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(cancelSearch:)];
    return YES;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    self.navigationItem.rightBarButtonItem = _dropPinButton;
    [searchBar resignFirstResponder];
    
    MKMapRect mRect = _mapView.visibleMapRect;
    MKMapPoint neMapPoint = MKMapPointMake(MKMapRectGetMaxX(mRect), mRect.origin.y);
    MKMapPoint swMapPoint = MKMapPointMake(mRect.origin.x, MKMapRectGetMaxY(mRect));
    CLLocationCoordinate2D neCoord = MKCoordinateForMapPoint(neMapPoint);
    CLLocationCoordinate2D swCoord = MKCoordinateForMapPoint(swMapPoint);
    CLLocationDistance diameter = [self getDistanceFrom:neCoord to:swCoord];
    
    CLRegion *region = [[CLCircularRegion alloc] initWithCenter:_mapView.centerCoordinate radius:(diameter/1.5) identifier:@"mapWindow"];
    
    NSString *searchText = searchBar.text;
    [_geoCoder geocodeAddressString:searchText inRegion:region completionHandler:^(NSArray *placemarks, NSError *error) {
        if (error) {
            NSLog(@"ERROR when geocoding \"%@\": %@", searchText, error);
        }else if (placemarks.count>0){
            [self resetPointsOnMap];
            
            DPPoint *point;
            int i = 1;
            for (CLPlacemark *place in placemarks) {
                point = [[DPPoint alloc] init];
                point.type = DPPointTypeSearchResult;
                NSArray *addressLines = [place.addressDictionary objectForKey:@"FormattedAddressLines"];
                point.title = [addressLines objectAtIndex:0];
                NSString *address = @"";
                for (int i=1; i<addressLines.count; i++) {
                    NSString *line = [addressLines objectAtIndex:i];
                    address = [NSString stringWithFormat:@"%@, %@",address,line];
                }
                point.subtitle = [address substringFromIndex:2];
                [point setCoordinate:place.location.coordinate];
                
                [_mapView addAnnotation:point];
                i++;
            }
            [self zoomToShowAllAnnotations];
        }
    }];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    
}

#pragma mark - MKMapViewDelegate

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id < MKAnnotation >)annotation{
    if (![annotation isKindOfClass:[DPPoint class]]) return nil;
    MKPinAnnotationView *result = nil;
    DPPoint *point = annotation;

    result = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"place"];
    result.canShowCallout = YES;
    result.animatesDrop = YES;
    BOOL shouldMoveIt = YES;
    if (_delegate && [_delegate respondsToSelector:@selector(mapSelection:shouldMovePoint:)]) {
        shouldMoveIt = [_delegate mapSelection:self shouldMovePoint:point];
    }
    result.draggable = shouldMoveIt;

    BOOL canBeDeleted = YES;
    if (_delegate && [_delegate respondsToSelector:@selector(mapSelection:shouldDeletePoint:)]) {
        canBeDeleted = [_delegate mapSelection:self shouldDeletePoint:point];
    }
    if (canBeDeleted) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:[UIImage imageNamed:@"close_circle"] forState:UIControlStateNormal];
        button.frame = CGRectMake(0, 0, 22, 22);
        result.rightCalloutAccessoryView = button;
    }

    switch (point.type) {
        case DPPointTypeInitialPoint:
            result.pinColor = MKPinAnnotationColorRed;
            break;
        case DPPointTypeSearchResult:
            result.pinColor = MKPinAnnotationColorPurple;
            result.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeContactAdd];
            break;
            
        default:
            result.pinColor = MKPinAnnotationColorGreen;
            break;
    }
    
    return result;
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control{
    if ([view.annotation isKindOfClass:[DPPoint class]]){
        DPPoint *point = view.annotation;
        if (point.type==DPPointTypeSearchResult) {
            point.type=DPPointTypeDroppedPin;
            [_points addObject:point];
            if (_delegate && [_delegate respondsToSelector:@selector(mapSelection:didAddPoint:)]) {
                [_delegate mapSelection:self didAddPoint:point];
            }
        }else {
            [_points removeObject:point];
            if (_delegate && [_delegate respondsToSelector:@selector(mapSelection:didDeletePoint:)]) {
                [_delegate mapSelection:self didDeletePoint:point];
            }
        }
        [self resetPointsOnMap];
    }else{
        NSLog(@"Don't know what to do with: %@", view.annotation);
    }
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)annotationView didChangeDragState:(MKAnnotationViewDragState)newState fromOldState:(MKAnnotationViewDragState)oldState{
    DPPoint *point = annotationView.annotation;
    
    if (newState==MKAnnotationViewDragStateEnding) {
        CLLocation *loc = [[CLLocation alloc] initWithLatitude:point.coordinate.latitude longitude:point.coordinate.longitude];
        [_geoCoder reverseGeocodeLocation:loc completionHandler:^(NSArray *placemarks, NSError *error) {
            if (!error && placemarks.count>0) {
                CLPlacemark *place = [placemarks objectAtIndex:0];
                point.subtitle = ABCreateStringWithAddressDictionary(place.addressDictionary, NO);
            }
            if (_delegate && [_delegate respondsToSelector:@selector(mapSelection:didMovePoint:)]) {
                [_delegate mapSelection:self didMovePoint:point];
            }
        }];
    }
}

@end

@implementation DPPoint
@synthesize coordinate=_coordinate;

-(NSString *)subtitle{
    if (_subtitle && ![_subtitle isEqualToString:@""]) {
        return [_subtitle stringByReplacingOccurrencesOfString:@"\n" withString:@", "];
    }else{
        return [NSString stringWithFormat:@"%.4f, %.4f", _coordinate.latitude, _coordinate.longitude];
    }
}

- (void)setCoordinate:(CLLocationCoordinate2D)newCoordinate{
    _coordinate = newCoordinate;
}

-(NSString*)description{
    return [NSString stringWithFormat:@"%@ - %@ (%.4f, %.4f)", self.title, self.subtitle, _coordinate.latitude, _coordinate.longitude];
}

@end

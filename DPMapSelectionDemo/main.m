//
//  main.m
//  DPMapSelectionDemo
//
//  Created by Diego Peinador on 19/02/14.
//  Copyright (c) 2014 Diego Peinador. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DPAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([DPAppDelegate class]));
    }
}
